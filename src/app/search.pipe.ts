import { Pipe, PipeTransform } from '@angular/core';
import { IEmployee } from './employee';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(employees:IEmployee[],searchEmp:string):IEmployee[] {
    if(!employees||!searchEmp){
      return employees; 
    }
searchEmp=searchEmp.toLowerCase();
//
    return employees.filter(employee=>
      employee.name.toLowerCase().indexOf(searchEmp)!==-1);
  }

}
