import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-employees-listt',
  templateUrl: './employees-listt.component.html',
  styleUrls: ['./employees-listt.component.css']
})
export class EmployeesListtComponent implements OnInit {
 public employees=[];
 public searchEmployee:string;

  constructor(private _employeeService:EmployeeService,private router:Router) { }
getUsers(){
  this._employeeService.getEmployee()
    .subscribe(data=>{data.sort((a,b) => a.id < b.id ? -1 : 1);
      this.employees=data;});
}
del(id:number){
this._employeeService.deleteEmp(id).subscribe(data => {
  this.getUsers();
});
}

  ngOnInit() {
    this.getUsers();
  }
  
}
