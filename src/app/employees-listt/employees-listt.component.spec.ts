import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesListtComponent } from './employees-listt.component';

describe('EmployeesListtComponent', () => {
  let component: EmployeesListtComponent;
  let fixture: ComponentFixture<EmployeesListtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeesListtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesListtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
