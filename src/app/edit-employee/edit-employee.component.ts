import { Component, OnInit, Input } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { IEmployee } from '../employee';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {

  public id:number;
  public employee=new IEmployee();
  public user=new IEmployee();

  getForm(){
   // this.id=this._employeeService.get();
   this.activateRoute.params.subscribe(params => {
    this.id = params['id'];
    //console.log(this.id); // Print the parameter to the console. 
});
    this._employeeService.getEmp(this.id)
      .subscribe(data=>{this.employee=data;});
     
  }
  

  update(emp:IEmployee){
    emp.id=Number(this.id);
    this._employeeService.getEmp(this.id)
    .subscribe(data=>{this.employee=data;
      this.employee=emp;
      this._employeeService.updateEmp(emp.id,emp).subscribe((ret)=>{console.log(ret);});
      this.router.navigate(['/employees']);
     } );
  
  }
  constructor(private _employeeService:EmployeeService,private router:Router,private activateRoute:ActivatedRoute) { }

  ngOnInit(): void {
    
  this.getForm();
  }

}
