import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IEmployee } from './employee';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private _url:string="api/employees";
  headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json');
  httpOptions= {headers: this.headers};
  constructor(private http:HttpClient) { }

   

 public fid:number;

  getEmployee():Observable<IEmployee[]>{
    return this.http.get<IEmployee[]>(this._url);
  }

  AddEmployee(employee:IEmployee){
      let body = employee;
      console.log(body);
      return this.http.post<IEmployee>(this._url, body);
      
  }
  getEmp (id: number): Observable<IEmployee> {
    this.fid=id;
    const url = this._url+'/'+id;
    console.log(url);
    return this.http.get<IEmployee>(url);
    }

    updateEmp(empId:number,emp:IEmployee): Observable<IEmployee>{
     const url = this._url+'/'+empId;
  return this.http.put<IEmployee>(url,emp,this.httpOptions);
    }
 
    deleteEmp (id: number): Observable<IEmployee> {
      const url = this._url+'/'+id;
      return this.http.delete<IEmployee>(url, this.httpOptions);
    }
}
