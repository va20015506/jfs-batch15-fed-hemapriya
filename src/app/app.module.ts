import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeesListtComponent } from './employees-listt/employees-listt.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import {HttpClientModule} from '@angular/common/http';
import { SearchPipe } from './search.pipe';
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import { EmpServiceService } from './emp-service.service';
import { EmployeeService } from './employee.service';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';
import { DetailsComponent } from './details/details.component';
@NgModule({
  declarations: [
    AppComponent,
    EmployeesListtComponent,
    AddEmployeeComponent,
    SearchPipe,
    EditEmployeeComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule ,
    AppRoutingModule,
   InMemoryWebApiModule.forRoot(EmpServiceService)
  ],
  providers: [EmpServiceService,EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
