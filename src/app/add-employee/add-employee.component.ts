import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  //public emp:IEmployee;

  constructor(private _employeeService:EmployeeService,private router: Router) { }
  
  add(emp:any){
    // this.emp.name=emp.value.name;
    // this.emp.location=emp.value.location;
    // this.emp.email=emp.value.email;
    // this.emp.mobile=emp.value.mobile;
    // console.log(this.emp);
  this._employeeService.AddEmployee(emp.value).subscribe((ret)=>{console.log(ret);});
  this.router.navigate(['/employees']);
  }
  ngOnInit() {
  }

}
