import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeesListtComponent } from './employees-listt/employees-listt.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';
import { DetailsComponent } from './details/details.component';

const routes: Routes = [
  {
    path: 'employees',
    component:EmployeesListtComponent
    
 },
 {
   path: 'addEmployee',
   component:AddEmployeeComponent
},
{
 path: 'editEmployee/:id',
 component:EditEmployeeComponent
},
{
path: 'details/:id',
component:DetailsComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
