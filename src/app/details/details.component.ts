import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { IEmployee } from '../employee';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  constructor(private _employeeService:EmployeeService,private router: Router,private activateRoute:ActivatedRoute) { }

  public employee=new IEmployee();
  public id:number;

  details()
  {
    this._employeeService.getEmp(this.id)
      .subscribe(data=>this.employee=data);
  }
  ngOnInit(): void {
    this.activateRoute.params.subscribe(params => {
      this.id = params['id']; 
  });
  this.details();
  }

}